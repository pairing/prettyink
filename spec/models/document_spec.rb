# frozen_string_literal: true

# == Schema Information
#
# Table name: documents
#
#  id                 :uuid             not null, primary key
#  aasm_state         :string
#  error_message      :string
#  input_data         :json
#  version            :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  job_id             :string
#  render_template_id :uuid             not null
#
# Indexes
#
#  index_documents_on_render_template_id  (render_template_id)
#
# Foreign Keys
#
#  fk_rails_...  (render_template_id => render_templates.id)
#
require 'rails_helper'

RSpec.describe Document, type: :model do
  let(:render_template) { RenderTemplate.create!(name: 'foo', schema: { string: 'string' }) }

  describe 'rendering' do
    context 'when all data is correct' do
      xit 'renders a document correctly' do
        document = Document.create!(render_template: render_template)
        document.render!

        expect(document.failed?).to be_falsey
      end

      it 'starts a backgronud job' do
        document = Document.create!(render_template: render_template)
        allow(document).to receive(:validate_schema).and_return(true)
        document.render!

        expect(document.running?).to be_truthy
      end
    end
  end

  describe '#method' do
    context 'when situation' do
    end
  end
end
