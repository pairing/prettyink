# frozen_string_literal: true

module Details
  class BlankContainer < Hash
    def initialize; end

    def to_json(*_args)
      ''
    end
  end
end
