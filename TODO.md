

# Building the Pretty Docs Prototype 

- [ ] Multi Tenant system?
- [ ] Editor: be able to open it without fragments present
- [ ] Validate versions? https://code.dblock.org/2020/07/16/comparing-version-numbers-in-ruby.html 

## Email generation
- [x] Make emails for JJ
- [ ] Add premailer


## Jobsite integration
- [ ] Make config module with server + token variable
- [ ] Add class to get template via template-name + version
- [ ] try https://mjml.io or https://get.foundation/emails/docs/inky.html 
- [ ] Evaluate Inky for documents




# Optional tasks

- [ ] add nx logo --> make asset loader for logo (includes URL)
- [ ] add switch: handlebars+JSON to codemirror https://codemirror.net/doc/manual.html#modloader
- [ ] Add link to playground on the / page
- [ ] make JSON schema matching right away + add to playground
- [ ] https://www.pdfonline.com/convert-pdf-to-html/
- [ ] Fix skip_before_action :verify_authenticity_tokens
- [ ] debounce() --> change on typing
- [ ] Error handling when sending emails. It should throw an error!
- [ ] Testing: https://evilmartians.com/chronicles/system-of-a-test-setting-up-end-to-end-rails-testing 

## Feedback from Client B:
- [ ] dokumentübegreifende daten, vererbung (!!!)
- [ ] fuer firmen, die mehr als ein backendsystem nutzen
- [ ] bisschen WYSIWYG würde passen
- [ ] generell arbeiten sie ohne kommission, machen nur empfehlungen
- [ ] schwierig, kunden zu pitchen, aber wenn es bei 4 kunden gehen würde, 
      dann "wäre ich wer" und dann könnte ich es weiter verkaufen.

## Feedback from Client A:
- [ ]  Textbausteine. Beispiel Schaden: Baut sich die texte zusammen
- [ ]  Kein System am Markt hat diese Möglichkeit, diese Bausteine einzusetzen.
- [ ]  Was mir immer vorschwebte, ist dass das DocMangmtSystem von einer AI gesteuer wird. Da gibt's keine am Markt. 
- [ ]  Deswegen interessiert es uns als Service, weil wir da etwas spezifisches brauchen.
- [ ]  Mail-Vorlagenstruktur wäre eine sinnvolle erweiterung. wenn da was verändert wird, ist es produktionsaktiv, ohne dass eine Version angelegt wird. kein redaktionsprozess.


## Random notes
- [ ] https://www.betterstimulus.com/architecture/configurable-controllers.html
- [ ] Add Tenants: https://github.com/citusdata/activerecord-multi-tenant 
- [ ] Active Job guides: https://github.com/toptal/active-job-style-guide