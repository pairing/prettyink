const { environment } = require('@rails/webpacker')

const webpack = require('webpack')// Add an additional plugin of your choosing : ProvidePlugin
environment.plugins.prepend('Provide', new webpack.ProvidePlugin({
    $: 'jquery',
    JQuery: 'jquery',
    jquery: 'jquery',
    'window.Tether': "tether",
    Popper: ['popper.js', 'default'], // for Bootstrap 4
  })
)

const resolver = {
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  }
}
environment.config.merge(resolver)


// Required for vue-loader v15
const VueLoaderPlugin = require('vue-loader/lib/plugin')
environment.plugins.append(
  'VueLoaderPlugin',
  new VueLoaderPlugin()
)

environment.loaders.append('vue', {
  test: /\.vue$/,
  use: 'vue-loader'
})

environment.loaders.append('json', {
  test: /\.json$/,
  use: 'json-loader'
})

environment.loaders.append('yaml', {
  test: /\.yml$/,
  use: 'yaml-loader'
})


module.exports = environment
