# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users

  require 'sidekiq/web'
  authenticate :user, ->(u) { u.admin? } do
    mount Sidekiq::Web => '/sidekiq', as: :sidekiq

    namespace :admin do
      # TODO: administrate
    end

    namespace :manage do
      resources :fragments
      resources :render_templates do
        get 'editor', on: :member
        get 'preview', on: :member
      end
      resources :documents do
        get 'preview', on: :member
        get 'download', on: :member
      end
      root to: 'dashboard#index'
    end
  end

  resources :documents, only: %i[show retry create] do
    post 'retry', on: :member
  end

  get 'playground', to: 'welcome#playground'
  get 'iframe', to: 'welcome#iframe'

  root 'welcome#index'
end
