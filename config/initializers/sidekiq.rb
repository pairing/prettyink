# frozen_string_literal: true

if Rails.env.production? || Rails.env.staging?
  # from
  # http://aleksandr-rogachev-blog.com/2017/07/25/effective_deployment_of_rails_app_to_heroku_with_sidekiq_and_puma/

  redis_connect = { url: ENV['REDISTOGO_URL'] || ENV['REDIS_URL'], size: 1, password: ENV.fetch('REDIS_PASSWORD') }
  redis_connect[:password] = ENV['REDIS_PASSWORD'] {}

  Sidekiq.configure_client do |config|
    config.redis = redis_connect
  end

  Sidekiq.configure_server do |config|
    redis_connect['size'] = ENV['SIDEKIQ_DB_POOL'] || (Sidekiq.options[:concurrency] + 2)
    config.redis = redis_connect
  end
end

# Making sure it loaded the job. If not: crash, please
Rails.logger.info("DocumentGenerationJob Class: #{DocumentGenerationJob}")
