FROM ruby:2.6.6
# Ubuntu
RUN apt-get update -qq 

# RUN curl https://deb.nodesource.com/setup_12.x | bash
# RUN curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
# RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get install -y apt-transport-https ca-certificates postgresql-client git nodejs npm bash openssl
RUN apt-get install -y wkhtmltopdf


RUN gem update --system
RUN npm install yarn -g

ENV APP_HOME /app


ARG RAILS_ENV
ENV RAILS_ENV=$RAILS_ENV

ARG RAILS_MASTER_KEY
ENV RAILS_MASTER_KEY=$RAILS_MASTER_KEY

ARG POSTGRES_HOST
ENV POSTGRES_HOST=$POSTGRES_HOST

ARG POSTGRES_USER
ENV POSTGRES_USER=$POSTGRES_USER

ARG POSTGRES_PASSWORD
ENV POSTGRES_PASSWORD=$POSTGRES_PASSWORD

ARG REDIS_URL
ENV REDIS_URL=$REDIS_URL

ARG REDIS_PASSWORD
ENV REDIS_PASSWORD=$REDIS_PASSWORD  

ARG RAILS_SERVE_STATIC_FILES
ENV RAILS_SERVE_STATIC_FILES=$RAILS_SERVE_STATIC_FILES


RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/
RUN yarn install
RUN gem install bundler:2.1.4
RUN bundle config set without 'development test'
RUN bundle install

COPY . $APP_HOME

# RUN bundle exec rake yarn:install

RUN bundle exec rails webpacker:yarn_install
RUN yarn --version

RUN bundle exec rails assets:precompile

COPY config/docker/entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 80

COPY config/docker/start_rails_and_sidekiq.sh $APP_HOME/start_rails_and_sidekiq.sh
RUN chmod +x $APP_HOME/start_rails_and_sidekiq.sh

# CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
CMD $APP_HOME/start_rails_and_sidekiq.sh