class CreateRenderTemplates < ActiveRecord::Migration[6.0]
  def change

    create_table :render_templates, id: :uuid do |t|
      t.json :schema
      t.string :name

      t.timestamps
    end
  end
end
