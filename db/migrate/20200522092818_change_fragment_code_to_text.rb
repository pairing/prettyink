class ChangeFragmentCodeToText < ActiveRecord::Migration[6.0]
  def change
    change_column :fragments, :code, :text
  end
end
