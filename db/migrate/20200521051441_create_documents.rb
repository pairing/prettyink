class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents, id: :uuid do |t|
      t.json :input_data
      t.references :render_template, null: false, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
