class MakeRenderTemplateNameUniqe < ActiveRecord::Migration[6.0]
  def change
    add_index :render_templates, :name, unique: true
  end
end
