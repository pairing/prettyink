class AddDocumentState < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :aasm_state, :string
    add_column :documents, :error_message, :string
    add_column :documents, :job_id, :string
  end
end
