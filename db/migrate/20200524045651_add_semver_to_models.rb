class AddSemverToModels < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :version, :string
    add_column :render_templates, :version, :string
  end
end
