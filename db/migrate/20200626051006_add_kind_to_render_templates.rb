class AddKindToRenderTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :render_templates, :kind, :string
    RenderTemplate.update_all(kind: :pdf)
  end
end
