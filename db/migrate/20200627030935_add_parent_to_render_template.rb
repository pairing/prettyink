class AddParentToRenderTemplate < ActiveRecord::Migration[6.0]
  def change
    add_reference :render_templates, :parent, index: true, type: :uuid
  end
end
