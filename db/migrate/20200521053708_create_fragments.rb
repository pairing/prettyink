class CreateFragments < ActiveRecord::Migration[6.0]
  def change
    create_table :fragments, id: :uuid do |t|
      t.string :identifier
      t.string :kind
      t.json :code
      t.references :render_template, null: false, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
