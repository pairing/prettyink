# frozen_string_literal: true

class EditorReflex < ApplicationReflex
  include CableReady::Broadcaster

  def save_form
    fields = transform_to_keys(element.dataset.fields)

    @render_template = RenderTemplate.find(fields[:template_id])
    @document = @render_template.documents.first_or_create

    @preview_data = {}
    @preview_data.merge!(fields)
    html = @render_template.render(@preview_data)

    cable_ready['code-editor'].inner_html(
      selector: '.preview_html', # required - string containing a CSS selector or XPath expression
      html: html
    )

    cable_ready.broadcast
  end

  def focus_preview
    session[:editor_current_fragment] = nil
  end

  def overwrite_fragment
    template_id = element.dataset.render_template_id
    fragment_id = element.dataset.fragment_id
    parent_fragment = Fragment.find(fragment_id)

    overwritten_fragment = parent_fragment.dup
    overwritten_fragment.render_template_id = template_id
    overwritten_fragment.save!

    session[:editor_current_fragment] = overwritten_fragment.id
  end

  def focus_fragment
    fragment_id = element.dataset.fragment_id
    session[:editor_current_fragment] = fragment_id
    # edit_fragment = Fragment.find(fragment_id)
    # html = ApplicationController.render partial: 'manage/render_templates/editor/fragment', locals: { fragment: edit_fragment }
    # cable_ready['code-editor'].inner_html(
    #   selector: '#multi-editor', # required - string containing a CSS selector or XPath expression
    #   html: html
    # )
    # cable_ready.broadcast
  end

  def save_fragment
    fields = transform_to_keys(element.dataset.fields)

    @fragment = Fragment.find(fields[:fragment_id])
    @fragment.update!(code: fields[:code])
  end

  private def transform_to_keys(named_values)
    json = JSON.parse(named_values)
    json.each_with_object(ActiveSupport::HashWithIndifferentAccess.new(0)) do |data, hsh|
      hsh[data['name']] = data['value']
    end
  end
end
