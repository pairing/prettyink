# frozen_string_literal: true

class CounterReflex < ApplicationReflex
  include CableReady::Broadcaster

  def increment(step = 1)
    session[:count] = session[:count].to_i + step

    cable_ready['code-editor'].text_content(
      # selector: "#post-#{post.id}-likes",
      selector: '.counter',
      text: "...: #{session[:count]}"
    )
    cable_ready.broadcast
  end
end
