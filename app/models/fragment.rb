# frozen_string_literal: true

# == Schema Information
#
# Table name: fragments
#
#  id                 :uuid             not null, primary key
#  code               :text
#  identifier         :string
#  kind               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  render_template_id :uuid             not null
#
# Indexes
#
#  index_fragments_on_render_template_id  (render_template_id)
#
# Foreign Keys
#
#  fk_rails_...  (render_template_id => render_templates.id)
#
class Fragment < ApplicationRecord
  belongs_to :render_template
  validates :kind, presence: true
  validates :render_template, presence: true

  scope :partials, -> { where.not(identifier: 'layout') }

  def code_preview
    code.gsub('{{', '{ {').gsub('}}', '} }')
  end

  def extract_variables
    variables = []
    nodes = Liquid::Template.parse(code).root.nodelist
    nodes.each do |n|
      variables << n.name.name if n.is_a? Liquid::Variable
    end
    # Return
    variables.uniq
  end

  def editor_around_html(inside, initiator: nil)
    own = initiator == render_template
    <<-HEREDOC
      <div class='partial-start #{own ? 'own' : 'from-parent'}'>
        <a href='#' class='action' 
          data-controller='editor' 
          data-fragment-id='#{id}'
          data-action='click->editor#focusFragment'>edit</a>
      </div>
      #{inside}
    HEREDOC
  end

  def editor_mode
    case kind
    when 'css'
      'css'
    when 'liquid'
      'handlebars'
    end
  end
end
