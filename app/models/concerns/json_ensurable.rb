# frozen_string_literal: true

module JsonEnsurable
  extend ActiveSupport::Concern
  included do
    # We dynamically use this below by applying class_eval
  end

  # https://stackoverflow.com/questions/38596934/define-instance-methods-in-concern-dynamically-on-rails
  class_methods do
    def ensure_json(field)
      class_eval <<-RUBY, __FILE__, __LINE__ + 1

        before_save :ensure_json_#{field}
        def ensure_json_#{field}
          return {} if #{field}.nil?
          self.#{field} = JSON.parse(#{field}) unless #{field}.is_a?(Hash)
        end

        def #{field}_json
          self.#{field}.to_json
        end

      RUBY
    end
  end
end
