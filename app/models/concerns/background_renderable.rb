# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
# rubocop:disable Metrics/AbcSize
module BackgroundRenderable
  extend ActiveSupport::Concern

  included do
    include AASM
    # We dynamically use this below by applying class_eval

    aasm do
      state :created, initial: true
      state :running
      state :completed
      state :aborted

      # to be used by the controller
      event :render, error: :abort_with_error! do
        before do
          validate_schema
          start_background_job
        end
        transitions from: %i[created completed], to: :running
      end

      event :complete do
        transitions from: :running, to: :completed
      end

      event :abort do
        transitions from: %i[created running], to: :aborted
      end

      event :reset do
        before do
          self.error_message = ''
          result_file.purge
          remove_sidekiq_job
        end
        transitions from: %i[created completed aborted running], to: :created
      end
    end

    def validate_schema
      # raise 'Schema validation not implemented yet'
    end

    def start_background_job
      # for testing purposes
      # job = DocumentGenerationJob.set(wait_until: Time.zone.now + 20.seconds).perform_later(self)
      job = DocumentGenerationJob.perform_later(self)
      # job = DocumentGenerationJob.perform_now(self)
      update(job_id: job.job_id)
    end

    def abort_with_error!(error)
      return if reload.completed?

      self.error_message = error.to_s
      abort! unless aborted?
    end

    def check_if_active! # rubocop:disable Metrics/CyclomaticComplexity
      return unless running?
      # respect the small window where the job is not in sidekiq yet
      return unless updated_at < Time.zone.now - 10.seconds

      render_jobs_args = []
      sets = [Sidekiq::Queue, Sidekiq::RetrySet, Sidekiq::ScheduledSet].map(&:new) + Sidekiq::Queue.all
      sets.map do |set|
        set.map do |job|
          next unless job.args[0]['job_class'] == 'DocumentGenerationJob'

          render_jobs_args << job.args[0]['arguments'].first['_aj_globalid']
        rescue StandardError
          nil
        end
      end

      return if render_jobs_args.include?(to_global_id.to_s)

      # check dead set
      job = Sidekiq::DeadSet.new.find_job(job_id)
      return abort_with_error!('job dead') if job.present?

      return unless reload.running?

      # finally: some issue has appeared:
      abort_with_error!('job vanished')
    end

    def remove_sidekiq_job
      glob_param = to_global_id.to_s

      Sidekiq::Queue.new.find_job(job_id)&.delete

      sets = [Sidekiq::Queue, Sidekiq::RetrySet, Sidekiq::ScheduledSet].map(&:new) + Sidekiq::Queue.all
      jobs = sets.map(&:map)

      jobs.each do |job|
        next unless job.args[0]['job_class'] == 'DocumentGenerationJob'
        next unless glob_param == job.args[0]['arguments'].first['_aj_globalid']

        job.delete
      rescue StandardError
        nil
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
# rubocop:enable Metrics/AbcSize
