# frozen_string_literal: true

# == Schema Information
#
# Table name: documents
#
#  id                 :uuid             not null, primary key
#  aasm_state         :string
#  error_message      :string
#  input_data         :json
#  version            :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  job_id             :string
#  render_template_id :uuid             not null
#
# Indexes
#
#  index_documents_on_render_template_id  (render_template_id)
#
# Foreign Keys
#
#  fk_rails_...  (render_template_id => render_templates.id)
#
class Document < ApplicationRecord
  # all aasm states in here
  include BackgroundRenderable

  include JsonEnsurable
  ensure_json :input_data

  has_one_attached :result_file
  belongs_to :render_template

  validates :render_template, presence: true

  def process!
    html = render_template.render(input_data)
    pdf = WickedPdf.new.pdf_from_string(html)
    result_file.attach(io: StringIO.new(pdf), filename: "#{id}.pdf", content_type: 'application/pdf')
  end

  def reset!
    update(aasm_state: :running)
    result_file&.purge
  end

  def preview
    render_template.render(input_data)
  end
end
