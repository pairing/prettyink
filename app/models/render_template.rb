# frozen_string_literal: true

# == Schema Information
#
# Table name: render_templates
#
#  id         :uuid             not null, primary key
#  kind       :string
#  name       :string
#  schema     :json
#  version    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :uuid
#
# Indexes
#
#  index_render_templates_on_name       (name) UNIQUE
#  index_render_templates_on_parent_id  (parent_id)
#
class RenderTemplate < ApplicationRecord
  acts_as_tree order: 'name'
  extend ActsAsTree::TreeView
  extend ActsAsTree::TreeWalker

  include JsonEnsurable
  ensure_json :schema

  validates :schema, presence: true
  validates :name, presence: true, uniqueness: true

  has_many :fragments, dependent: :destroy
  has_many :documents, dependent: :nullify

  enum kinds: { pdf: 0, email: 1 }
  validates :kind, inclusion: { in: kinds }

  def self.select_options
    result = []
    result << ['none (root)', nil]
    RenderTemplate.walk_tree do |template, level|
      result << ["#{'-' * level}#{template.name}", template.id]
    end
    result
  end

  def to_s
    name
  end

  def walk_branch
    part_tree = []
    walk_tree do |node, level|
      part_tree << [node, level]
    end

    # Siblings does no work yet
    # siblings = RenderTemplate.where(parent: self.parent) if self.parent
    # siblings&.each do |sibling|
    #   next if siblings == self
    #   part_tree << [sibling, 0]
    # end

    current = self
    while current.present?
      part_tree.each { |item| item[1] += 1 }
      part_tree.unshift([current, 0])

      current = current.parent
    end

    part_tree.each do |node, level|
      yield node, level
    end
  end

  def inherited_fragments
    # loop from here to parent
    # add templates that are not there yet
    frag_hash = {}
    current = self
    while current.present?
      current.fragments.each do |fragment|
        next if frag_hash[fragment.identifier]

        frag_hash[fragment.identifier] = fragment.id
      end
      current = current.parent
    end

    Fragment.where(id: frag_hash.values)
  end

  def extract_variables
    vars = inherited_fragments.map(&:extract_variables).flatten
    vars.reject! { |var| var.match?(/^include/) }

    vars.uniq
  end

  def dummy_preview_data
    Hash[*extract_variables.collect { |v| [v, "{{#{v}}}"] }.flatten]
  end

  def render(input_data, edit: false)
    raise 'no layout fragment defined' if layout_fragment.nil?

    includes_hash = ActiveSupport::HashWithIndifferentAccess.new
    inherited_fragments.partials.each do |fragment|
      liquid = Liquid::Template.parse(fragment.code)
      html = liquid.render(input_data).html_safe # rubocop:disable Rails/OutputSafety
      html = fragment.editor_around_html(html, initiator: self) if edit
      includes_hash["include_#{fragment.identifier}"] = html
    end

    liquid = Liquid::Template.parse(layout_fragment.code) # Parses and compiles the template

    includes_hash.merge!(input_data) if input_data.present?
    output = liquid.render(includes_hash).html_safe # rubocop:disable Rails/OutputSafety
    output = layout_fragment.editor_around_html(output, initiator: self) if edit
    output
  end

  def layout_fragment
    inherited_fragments.find_by(identifier: 'layout')
  end
end
