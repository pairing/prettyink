# frozen_string_literal: true

class Manage::PeopleController < ApplicationController
  before_action :set_person, only: %i[show edit update destroy]

  # GET /people
  # GET /people.json
  def index
    @people = Person.all
  end

  # GET /people/1
  # GET /people/1.json
  def show; end

  def preview
    @person = Person.find(params[:id])
    render '/cvs/show', layout: 'cvs'
  end

  # GET /people/new
  def new
    @person = Person.new
  end

  # GET /people/1/edit
  def edit; end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)

    respond_to do |format|
      if @person.save
        format.html { redirect_to [:manage, @person], notice: 'Person was successfully created.' }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to [:edit, :manage, @person], notice: 'Person was successfully updated.' }
        format.json { render :edit, status: :ok, location: @person }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to manage_people_url, notice: 'Person was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  private def set_person
    @person = Person.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  private def person_params
    params.require(:person).permit(:last_name, :first_name, :details, :timezone, :tz_offset)
  end
end
