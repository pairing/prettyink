# frozen_string_literal: true

module Manage
  class FragmentsController < ApplicationController
    include CableReady::Broadcaster

    before_action :set_fragment, only: %i[show edit update destroy]

    # GET /fragments
    # GET /fragments.json
    def index
      redirect_to manage_render_templates_path
    end

    # GET /fragments/1
    # GET /fragments/1.json
    def show; end

    # GET /fragments/new
    def new
      @fragment = Fragment.new(kind: 'liquid')
      @fragment.render_template_id = params[:render_template_id]
    end

    # GET /fragments/1/edit
    def edit; end

    # POST /fragments
    # POST /fragments.json
    def create
      @fragment = Fragment.new(fragment_params)

      respond_to do |format|
        if @fragment.save
          format.html { redirect_to [:manage, @fragment], notice: 'Fragment was successfully created.' }
          format.json { render :show, status: :created, location: @fragment }
        else
          format.html { render :new }
          format.json { render json: @fragment.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /fragments/1
    # PATCH/PUT /fragments/1.json
    def update
      respond_to do |format|
        if @fragment.update(fragment_params)
          format.html { redirect_to [:edit, :manage, @fragment], notice: 'Fragment was successfully updated.' }
          format.json { render :show, status: :ok, location: @fragment }
        else
          format.html { render :edit }
          format.json { render json: @fragment.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /fragments/1
    # DELETE /fragments/1.json
    def destroy
      @fragment.destroy
      respond_to do |format|
        format.html { redirect_to manage_fragments_url, notice: 'Fragment was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    private def set_fragment
      @fragment = Fragment.find(params[:id])
      @count = session[:count].to_i
    end

    # Only allow a list of trusted parameters through.
    private def fragment_params
      params.require(:fragment).permit(:kind, :identifier, :code, :render_template_id)
    end
  end
end
