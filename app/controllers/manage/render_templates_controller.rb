# frozen_string_literal: true

module Manage
  class RenderTemplatesController < ApplicationController
    before_action :set_template, only: %i[show edit editor update destroy preview]

    # GET /templates
    # GET /templates.json
    def index
      @render_templates = RenderTemplate.all
    end

    # GET /templates/1
    def show; end

    # TODO: maybe remove ???
    # GET /templates/1
    def preview
      @document = @render_template.documents.first_or_create
      @preview_data = {}
      render html: @document.preview
    end

    # GET /templates/new
    def new
      @render_template = RenderTemplate.new
      return unless params[:parent_id]

      parent = RenderTemplate.find(params[:parent_id])
      @render_template = parent.dup
      @render_template.parent = parent
      @render_template.name = "#{parent.name}-child"
    end

    # GET /templates/1/edit
    def edit; end

    # GET /templates/1/edit
    def editor
      @edit_fragment = Fragment.where(id: session[:editor_current_fragment]).first if session[:editor_current_fragment]

      @preview_data ||= @render_template.dummy_preview_data

      render layout: 'editor'
    end

    # POST /templates
    # POST /templates.json
    def create
      @render_template = RenderTemplate.new(template_params)

      respond_to do |format|
        if @render_template.save
          format.html { redirect_to [:manage, @render_template], notice: 'RenderTemplate was successfully created.' }
          format.json { render :show, status: :created, location: @render_template }
        else
          format.html { render :new }
          format.json { render json: @render_template.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /templates/1
    # PATCH/PUT /templates/1.json
    def update
      respond_to do |format|
        if @render_template.update(template_params)
          format.html { redirect_to [:manage, @render_template], notice: 'RenderTemplate was successfully updated.' }
          format.json { render :show, status: :ok, location: @render_template }
        else
          format.html { render :edit }
          format.json { render json: @render_template.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /templates/1
    # DELETE /templates/1.json
    def destroy
      @render_template.destroy
      respond_to do |format|
        format.html { redirect_to manage_templates_url, notice: 'RenderTemplate was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    private def set_template
      @render_template = RenderTemplate.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    private def template_params
      params.require(:render_template).permit(:schema, :name, :version, :kind, :parent_id)
    end
  end
end
