# frozen_string_literal: true

class Manage::DashboardController < ApplicationController
  def index; end
end
