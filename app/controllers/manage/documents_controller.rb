# frozen_string_literal: true

module Manage
  class DocumentsController < ApplicationController
    before_action :set_document, only: %i[show edit update destroy preview download]

    # GET /manage/documents
    # GET /manage/documents.json
    def index
      @documents = Document.order(created_at: :desc).page(params[:page]).per(10)
    end

    # GET /manage/documents/1
    # GET /manage/documents/1.json
    def show; end

    def preview
      render html: @document.preview
    end

    def download
      filename = @document.download

      send_data File.read(filename), filename: "#{@document.id}.pdf"
    end

    # GET /manage/documents/new
    def new
      @document = Document.new
    end

    # GET /manage/documents/1/edit
    def edit; end

    # POST /manage/documents
    # POST /manage/documents.json
    def create
      @document = Document.new(document_params)

      respond_to do |format|
        if @document.save
          format.html { redirect_to [:manage, @document], notice: 'Document was successfully created.' }
          format.json { render :show, status: :created, location: @document }
        else
          format.html { render :new }
          format.json { render json: @document.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /manage/documents/1
    # PATCH/PUT /manage/documents/1.json
    def update
      respond_to do |format|
        if @document.update(document_params)
          format.html { redirect_to [:manage, @document], notice: 'Document was successfully updated.' }
          format.json { render :show, status: :ok, location: @document }
        else
          format.html { render :edit }
          format.json { render json: @document.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /manage/documents/1
    # DELETE /manage/documents/1.json
    def destroy
      @document.destroy
      respond_to do |format|
        format.html { redirect_to manage_documents_url, notice: 'Document was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    private def set_document
      @document = Document.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    private def document_params
      params.require(:document).permit(:input_data, :render_template_id, :version)
    end
  end
end
