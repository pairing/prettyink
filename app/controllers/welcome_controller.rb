# frozen_string_literal: true

class WelcomeController < ApplicationController
  skip_before_action :authenticate_user!

  def index; end

  def iframe
    contract_number = params[:contract_id]
    # TODO: remove after demo!
    @token = User.last.api_token

    template = RenderTemplate.where(name: 'local-kh-pdf1').order(:updated_at).last
    @template = template.name
    @version = template.version

    @data = {
      contract_number: contract_number,
      first_name: 'SCIP',
      last_name: 'Customer'
    }

    response.headers.except! 'X-Frame-Options'
    response.headers['X-Permitted-Cross-Domain-Policies'] = 'all'

    render layout: nil
  end

  def playground
    @document = Document.new

    @sample_templates = RenderTemplate.last(5).map do |template|
      {
        name: template.name,
        version: template.version,
        schema: template.schema.to_json
      }
    end
  end
end
