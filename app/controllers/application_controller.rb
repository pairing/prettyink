# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  before_action :set_layout

  private def set_layout
    if params[:iframe]

      # should also have this to expand the height of the parent frame
      # https://medium.com/better-programming/how-to-automatically-resize-an-iframe-7be6bfbb1214
      response.headers['Access-Control-Allow-Origin'] = '*'
      response.headers['X-Frame-Options'] = 'allow-from *'
      # response.headers['Content-Security-Policy'] = "default-src  *";
      self.class.layout 'iframe'
    else
      self.class.layout 'application'
    end
    #  self.class.layout ( params['iframe'] == 'true' ? 'vendored' :  'application')
  end
end
