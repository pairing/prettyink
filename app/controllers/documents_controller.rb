# frozen_string_literal: true

# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
class DocumentsController < ApplicationController
  # TODO: remove when going live
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!

  # GET /documents/1
  # GET /documents/1.json
  def show
    # TODO: compare token!
    @document = Document.find(params[:id])
    @document.check_if_active!

    response = { state: @document.aasm_state }

    response[:url] = url_for(@document.result_file) if @document.completed? && @document.result_file.attached?
    response[:error] = @document.error_message if @document.aborted?
    render json: response
  end

  # POST /documents/1/retry
  # POST /documents/1/retry.json
  def retry
    @document = Document.find(params[:id])
    @document.reset
    @document.render!

    response = { state: @document.aasm_state }
    response[:error] = @document.error_message if @document.aborted?
    render json: response
  end

  # POST /documents/
  def create
    # params = params.permit(:token, :data, :template, :version)
    authorized = params[:token] == User.last.api_token
    response = {}

    if authorized
      @template = RenderTemplate.where(name: params[:template], version: params[:version]).first
      response[:error] = 'Template not found' if @template.nil?
      input_data = JSON.parse(params[:data].to_s)

      @document = Document.new(render_template: @template, input_data: input_data, version: params[:version])
      response[:html] = @document.preview if @template.kind == 'email'
    else
      response[:error] = 'Unauthorized'
    end

    if response[:error].blank?
      if @document.save
        response[:id] = @document.id
        @document.render!
        response[:state] = @document.aasm_state
      else
        response[:error] = @document.error_message
      end
    end

    render json: response
  end
end
# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/MethodLength
