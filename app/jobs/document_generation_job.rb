# frozen_string_literal: true

class DocumentGenerationJob < ApplicationJob
  queue_as :prettyink_default

  # TODO: make sure to stick to the guidelines: https://github.com/toptal/active-job-style-guide
  def perform(document)
    document.reset! # just to make sure

    document.process!

    # raise 'ActiveStorage error: file was not attached' unless document.reload.result_file.attached?

    document.complete!
    document.update(job_id: nil)
  rescue StandardError => e
    document.abort_with_error!(e)
  end

  # this was for testing only...
  private def mock_delay
    4.downto(0) do |i|
      Rails.logger.debug "#{i}: #{document.aasm_state}: sidekiq=#{job_id} doc=#{document.job_id}"
      sleep 2
    end
  end
end
