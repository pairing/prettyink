# frozen_string_literal: true

json.partial! 'templates/template', template: @render_template
