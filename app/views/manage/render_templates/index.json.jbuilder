# frozen_string_literal: true

json.array! @render_templates, partial: 'templates/template', as: :template
