# frozen_string_literal: true

json.extract! document, :id, :input_data, :template_id, :created_at, :updated_at
json.url document_url(document, format: :json)
