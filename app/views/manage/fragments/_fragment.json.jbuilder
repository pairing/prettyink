# frozen_string_literal: true

json.extract! fragment, :id, :kind, :layout, :code, :render_template_id, :created_at, :updated_at
json.url fragment_url(fragment, format: :json)
