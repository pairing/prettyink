# frozen_string_literal: true

json.array! @fragments, partial: 'fragments/fragment', as: :fragment
