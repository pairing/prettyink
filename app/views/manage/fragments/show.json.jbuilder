# frozen_string_literal: true

json.partial! 'fragments/fragment', fragment: @fragment
