import ApplicationController from './application_controller'
import Vue from 'vue/dist/vue.esm'

/* This is the custom StimulusReflex controller for ExampleReflex.
 * Learn more at: https://docs.stimulusreflex.com
 */
export default class extends ApplicationController {
  /* Reflex specific lifecycle methods.
   * Use methods similar to this example to handle lifecycle concerns for a specific Reflex method.
   * Using the lifecycle is optional, so feel free to delete these stubs if you don't need them.
   *
   * Example:
   *
   *   <a href="#" data-reflex="ExampleReflex#example">Example</a>
   *
   * Arguments:
   *
   *   element - the element that triggered the reflex
   *             may be different than the Stimulus controller's this.element
   *
   *   reflex - the name of the reflex e.g. "ExampleReflex#example"
   *
   *   error - error message from the server
   */

  // beforeUpdate(element, reflex) {
  //  element.innerText = 'Updating...'
  // }

  // updateSuccess(element, reflex) {
  //   element.innerText = 'Updated Successfully.'
  // }

  // updateError(element, reflex, error) {
  //   console.error('updateError', error);
  //   element.innerText = 'Update Failed!'
  // }


  save(event) {
    event.preventDefault()

    // TODO: DRY
    let element = document.getElementById('editor-relevant-form')
    let fields = document.getElementsByClassName('editor-relevant')
    fields = Array.from(fields).map((input) => {
      return { name: input.name, value: input.value}
    })

    element.dataset.fields = JSON.stringify(fields)

    this.stimulate('EditorReflex#save_form', element)
  }

  saveFragment(event) {
    event.preventDefault()
    let element = document.getElementById('editor-relevant-form')
    let fields = document.getElementsByClassName('editor-relevant')
    fields = Array.from(fields).map((input) => {
      return { name: input.name, value: input.value}
    })

    element.dataset.fields = JSON.stringify(fields)

    this.stimulate('EditorReflex#save_fragment', element)
  }

  focusFragment(event) {
    event.preventDefault()
    let element = event.target
    this.stimulate('EditorReflex#focus_fragment', element)
  }

  overwriteFragment(event) {
    event.preventDefault()
    let element = event.target
    this.stimulate('EditorReflex#overwrite_fragment', element)
  }

  
  afterFocusFragment(element, reflex) {
    this.reloadVue()
  }
  afterSaveFragment(element, reflex) {
    this.reloadVue()
  }
  afterOverwriteFragment(element, reflex) {
    this.reloadVue()
  }


  reloadVue() {
    const app = new Vue({
      el: '[data-behavior="vue"]',
      delimiters: ["<%","%>"],
      provide: function () {
        return {
        }
      },

    });
    window.app = app;
  }

}
