// The channel responsible for the preview/edit code

import CableReady from 'cable_ready'
import consumer from './consumer'

consumer.subscriptions.create('CodeEditorChannel', {
  received (data) {
    if (data.cableReady) CableReady.perform(data.operations)
  }
})
