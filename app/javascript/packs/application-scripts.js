// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start() 
require("channels")

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

require("trix")
require("@rails/actiontext")

var jQuery = require("jquery")// import jQuery from "jquery";
global.$ = global.jQuery = jQuery;
window.$ = window.jQuery = jQuery;require('bootstrap');

// Stimulus Reflex
import "controllers"


import Vue from 'vue/dist/vue.esm'
// Vue.config.delimiters = ['${', '}']

import Turbolinks from 'turbolinks';
Turbolinks.start();


import VueTimers from 'vue-timers'
Vue.use(VueTimers)

import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)


// -------------- Own components --------------
import CodeEditor from '../components/CodeEditor.vue'
Vue.component('code-editor', CodeEditor)
import DownloadButton from '../components/DownloadButton.vue'
Vue.component('download-button', DownloadButton)
import PlaygroundCreateDocument from '../components/playground/CreateDocument.vue'
Vue.component('playground-create-document', PlaygroundCreateDocument)
import PlaygroundGetStatus from '../components/playground/GetStatus.vue'
Vue.component('playground-get-status', PlaygroundGetStatus)
import RawCreateButton from '../components/playground/RawCreateButton.vue'
Vue.component('raw-create-button', RawCreateButton)


document.addEventListener('turbolinks:load', () => {
  const app = new Vue({
    el: '[data-behavior="vue"]',
    delimiters: ["<%","%>"],
    provide: function () {
      return {
      }
    },

  });

  window.app = app;
});
