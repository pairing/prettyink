# frozen_string_literal: true

module ApplicationHelper
  def url_or_nil(document)
    return nil unless document.completed?
    return nil unless document.result_file.attached?

    url_for(document.result_file)
  end

  def deployment_info
    # test:
    # ENV['DEPLOY_TIME'] = 'Mon Jul 13 2020 08:18:37 GMT+0000 (Coordinated Universal Time)'

    info = ["<b>#{Rails.env}</b>"]
    deployed_at = DateTime.parse(ENV['DEPLOY_TIME']) if ENV['DEPLOY_TIME']
    if deployed_at
      info << 'deployed at <br />'
      info << "<b>#{l(deployed_at, format: :short)} </b>"
      info << "(#{time_ago_in_words(deployed_at)} ago)"
    end

    info.join(' ').html_safe # rubocop:disable Rails/OutputSafety
  end
end
