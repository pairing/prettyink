# frozen_string_literal: true

class CodeEditorChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'code-editor'
  end
end
