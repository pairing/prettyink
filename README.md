# README

This is a private repo. If you got this you should know why :)


## For new developers

Before asking you should be able to

- 1. set up the database, migrate
- 2. run the tests (*don't skip this step*)
- 3. add seeds and see the interface on localhost


## 1. Setup gems etc

Have these requirements ready: Ruby `2.6.6`, PostgreSql `10+`, Redis `5.0.8+`

Obviously: `bundle install`, then `yarn install --check-files`

Create the database. I attach this script to have it handy faster


```sh
dropdb prettydocs_development --if-exists  && \
createdb prettydocs_development && \
psql -c "DROP ROLE IF EXISTS prettydocs_development; CREATE USER prettydocs_development WITH PASSWORD 'prettydocs_development'; ALTER DATABASE prettydocs_development OWNER TO prettydocs_development; ALTER USER prettydocs_development CREATEDB; " prettydocs_development  && \
RAILS_ENV=development rails db:migrate

dropdb prettydocs_test --if-exists  && \
createdb prettydocs_test && \
psql -c "DROP ROLE IF EXISTS prettydocs_test; CREATE USER prettydocs_test WITH PASSWORD 'prettydocs_test'; ALTER DATABASE prettydocs_test OWNER TO prettydocs_test;  ALTER USER prettydocs_test CREATEDB; " prettydocs_test  && \
RAILS_ENV=test rails db:migrate
```

Then migrate: `rake db:create && rake db:migrate && rake db:seed`

## 2. Tests

Run `rspec` and make sure it all runs green

## 3. Run on localhost

Ideally you install `foreman` (or `overmind` if you are on mac), then open this in one tab:

- Tab 1: `bovermind start -f Procfile.dev`
- Tab 2: `rails s`

Then go to `localhost:3000/manage` and log in via the data found in the `db/seeds.rb`



## Deployment instructions

Heroku did not accept the latest webpack, which had this dependency. So we rolled the version back.
```
error move-file@2.0.0: The engine "node" is incompatible with this module. Expected version ">=10.17". Got "10.15.3"
"move-file": "~1.2.0"
```



